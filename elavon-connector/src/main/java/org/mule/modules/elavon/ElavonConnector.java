package org.mule.modules.elavon;

import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;

import java.io.IOException;
import org.mule.api.annotations.ReconnectOn;
import org.mule.api.annotations.lifecycle.Start;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.RefOnly;
import org.mule.api.annotations.rest.HttpMethod;
import org.mule.elavon.client.ElavonTransactionClient;

import com.google.gson.JsonObject;

import org.mule.modules.elavon.config.ConnectorConfig;
import org.mule.elavon.xml.APIField;
import org.mule.elavon.xml.Transaction;
import org.mule.module.json.JsonData;

import java.util.List;
import java.util.Map;


@Connector(name="elavon", friendlyName="Elavon")
public class ElavonConnector {
    @Config
    ConnectorConfig config;
    
    private ElavonTransactionClient client;

    @Start
    public void initialize() {
    	client = new ElavonTransactionClient(config.getAddress());
    }
   
    
	/**
	* Description for name
	*
	* {@elavon-connector.xml ../../../doc/elavon-connector.xml cookbook:postTransaction}
	*
	*	@param param The comment for param
	*	@return return comment
	*/
	@Processor
    public String postTransaction(@Default("#[payload]") @RefOnly final JsonData params) throws Exception {
    	return client.postTransaction(params);
    }
    
    
    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
    }
    
}