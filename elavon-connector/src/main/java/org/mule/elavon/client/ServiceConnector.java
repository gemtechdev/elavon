package org.mule.elavon.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class ServiceConnector {
	private CloseableHttpClient httpClient;
	private String address;
	
	public ServiceConnector(String address) {
		this.httpClient = HttpClients.createDefault();
		this.address = address;
	}
	
	public String post(String data) {
		HttpPost httpPost = new HttpPost(this.address);
		StringEntity requestEntity = new StringEntity(data, ContentType.APPLICATION_XML);
		httpPost.addHeader("Content-type", "text/xml");
		
		httpPost.setEntity(requestEntity);
		try {
			HttpResponse rawResponse = this.httpClient.execute(httpPost);
			
			if (rawResponse.getStatusLine().getStatusCode() == 200) {
				// Successful
				BufferedReader rd = new BufferedReader(new InputStreamReader(rawResponse.getEntity().getContent()));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
				System.out.println("Response: " + result.toString());
				return result.toString();
			} else {
				System.out.println(String.format("[REQUEST ERROR; URL: %s, Status Code: %s", 
						this.address,
						rawResponse.getStatusLine().getStatusCode()));
			}
		} catch (IOException exception) {
			System.out.println(exception.toString());
		} finally {
			httpPost.releaseConnection();
		}
		
		return null;
	}
}
