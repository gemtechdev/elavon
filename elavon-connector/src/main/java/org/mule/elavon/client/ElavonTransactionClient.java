package org.mule.elavon.client;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;

import org.mule.elavon.xml.APIField;
import org.mule.elavon.xml.Transaction;

import org.mule.module.json.JsonData;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Handles Elavon transactions
 * @author William Cliffe
 *
 */
public class ElavonTransactionClient {
	private List<APIField> xmlDocument = new ArrayList<>();
	private ServiceConnector serverEndpoint;
	
	public ElavonTransactionClient(String address) {
		serverEndpoint = new ServiceConnector(address);
	}
	
	/**
	 * Post transaction items.
	 */
	public String postTransaction(JsonData jsonData) throws Exception {
		String xml = prepareTransaction(jsonData);
		return serverEndpoint.post(xml);		
	}
	
	private String prepareTransaction(JsonData jsonData) throws Exception {
		List<APIField> apiFields = new ArrayList<>();
		JsonObject obj = new Gson().fromJson(jsonData.toString(), JsonObject.class);
		for (Map.Entry<String, JsonElement> entry: obj.entrySet()) {
			String key = entry.getKey();
			JsonElement element = entry.getValue();
			
			APIField apiField = new APIField();
			apiField.fieldNumber = key;
			apiField.fieldValue = element.getAsString();
			apiFields.add(apiField);
		}

		
		StringWriter writer = new StringWriter();
		Transaction transaction = new Transaction();
		transaction.apiFields = apiFields;
		JAXBContext jc = JAXBContext.newInstance(Transaction.class);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(transaction, writer);
		return writer.toString();
	}
}
