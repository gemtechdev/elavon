package org.mule.elavon.xml;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "API_Field")
public class APIField {
	
	@XmlElement(name="Field_Number")
	public String fieldNumber;
	
	@XmlElement(name="Field_Value")
	public String fieldValue;
	
	public APIField() {
	}
}
