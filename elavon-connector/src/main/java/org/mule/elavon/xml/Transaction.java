package org.mule.elavon.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Transaction {
	@XmlElement(name="API_Field")
	public List<APIField> apiFields = new ArrayList<>();
	
	public Transaction() {
		
	}
}
